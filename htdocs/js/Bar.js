define("Bar", ["jquery", "jsb"], function ($, jsb) {
    "use strict";

    var Bar = function (dom_element, options) {

        jsb.on("value:calculated", function (new_value) {
            $(dom_element).find("input").val(new_value);
        });
    };

    return Bar;
});