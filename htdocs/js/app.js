requirejs({
    "baseUrl": "./js/",
    "noGlobal": true,
    "paths": {
        "logging": "/bower_components/logging.js/logging",
        "jsb": "/bower_components/jsb/jsb",
        "jquery": "/bower_components/jquery/dist/jquery.min"
    },
    "shim": {},
    "include": [
        'app'
    ]
});

define('app', ['logging', 'jsb'], function(logging, jsb)
{
    var App = function()
    {
        logging.applyLogging(this, 'App');

        jsb.applyBehaviour(document.body);

        this.initializeEventListeners();
    };

    App.prototype.initializeEventListeners = function()
    {
        jsb.on("foo:clicked", function () {
           logging.logDebug("Foo clicked")
        });

    };

    return new App();
});