define("Foo", ["jquery", "jsb"], function ($, jsb) {
    "use strict";

    var Foo = function (dom_element, options) {

        $(dom_element).find(".js-doit").click(function() {
            jsb.fireEvent("foo:clicked");
        });

    };

    return Foo;
});