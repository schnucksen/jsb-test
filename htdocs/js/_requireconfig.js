requirejs({
    "baseUrl": "./js/",
    "noGlobal": true,
    "paths": {
        "logging": "bower_components/logging.js/logging",
        "jsb": "bower_components/jsb/jsb"
    },
    "shim": {},
    "include": [
        'app'
    ]
});