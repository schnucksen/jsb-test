'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({

        connect: {
            server: {
                options: {
                    port: 9100,
                    base: 'htdocs',
                    keepalive: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('build', [
        'connect:server'
    ]);

    grunt.registerTask('default', [
        'build'
    ]);

};
